var counter = 0;

window.addEventListener("load", () => {

    document.body.style.backgroundColor = 'silver';

    // edit style from event handlers
    var divEvents = document.querySelectorAll('div.events');
    divEvents[0].style = 'padding: 10px; background-color: #21d04c; width: fit-content';

    // listener
    document.getElementById('buttonEvent').onclick =
        () => document.getElementById('pEvent').innerHTML = ';-)';

    document.getElementById('btnEvent')
        .addEventListener("click", btnClicked);



});

function move() {
    var elem = document.getElementById("animate");
    var pos = 0;
    var id = setInterval(frame, 5);

    function frame() {
        if (pos == 350) {
            clearInterval(id);
        } else {
            pos++;
            elem.style.top = pos + "px";
            elem.style.left = pos + "px";
        }
    }
}

function bodyLoaded() {
    console.log('bodyLoaded');
}

function mOver(element) {
    console.log(element);
    console.log(element.innerHTML);

    element.innerHTML = 'Thank you';
}

function mOut(obj) {
    obj.innerHTML = "Mouse Over Me"
}

function btnClicked($event) {
    console.log($event);
    return undefined;
}

function addElement() {
    var div = document.getElementsByClassName('element-nodes')[0];

    var p = document.createElement('p');
    var node = document.createTextNode('This is new.' + counter++);
    p.appendChild(node);
    div.appendChild(p);
}

function removeChildren() {
    var parent = document.getElementsByClassName('element-nodes')[0];

    if (parent.childNodes) {
        var indexOfLastChild = parent.childNodes.length - 1;
        var lastChild = parent.childNodes[indexOfLastChild];
        parent.removeChild(lastChild);
    }

}