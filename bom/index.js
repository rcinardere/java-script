var dateInterval;

window.addEventListener("load", () => {

    var height = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
    var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;

    document.getElementById('windowHeight').innerHTML = 'height => ' + height + ' px';
    document.getElementById('windowWidth').innerHTML = 'width => ' + width + ' px';


    document.getElementById('screenHeight').innerHTML = 'screen.height: ' + window.screen.height;
    document.getElementById('screenWidth').innerHTML = 'screen.width: ' + window.screen.width;

});

function openWindow() {
    window.close();
}

function info() {
    var root = document.getElementsByClassName('location')[0];

    var windowNodes = [
        document.createTextNode('href: ' + window.location.href),
        document.createTextNode('path: ' + window.location.pathname),
        document.createTextNode('protocol: ' + window.location.protocol)
    ];

    for (var i = 0; i < windowNodes.length; i++) {
        var p = document.createElement('p');
        p.appendChild(windowNodes[i]);
        root.appendChild(p);
    }
}

function openDocument() {
    var value = document.getElementById('inDocument').value;
    var url = 'https://' + value;
    var confirmed = confirm('Leave window?').valueOf();
    console.log(confirmed);
    if (confirmed) {
        window.location.assign(url);
    }
}

function back() {
    window.history.back();
}

function forward() {
    window.history.forward();
}

function doTimeOut() {

    setTimeout(() => {
        alert('I have wait 3000 ms');
    }, 3000);

}

function doInterval() {

    dateInterval = setInterval(() => {
        var date = new Date();
        document.getElementById('pInterval').innerHTML = date.toLocaleTimeString();
    }, 1000);
}

function doClearInterval() {
    clearInterval(dateInterval);
}

function readAllCookies() {
    var cookie = document.cookie;
}

function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();

    document.cookie = 'username' + "=" + cvalue;
    console.log(document.cookie);
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

function checkCookie() {
    var user = getCookie("username");
    if (user != "") {
        alert("Welcome again " + user);
    } else {
        user = prompt("Please enter your name:", "");
        setCookie("username", user, 30);
    }
}