/**
 *
 */
class Car {
    constructor(brand, model) {
        this._brand = brand;
        this._model = model;
    }

    info() {
        return JSON.stringify(this);
    }


    get brand() {
        return this._brand;
    }

    set brand(value) {
        this._brand = value;
    }

    get model() {
        return this._model;
    }

    set model(value) {
        this._model = value;
    }
}