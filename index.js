var fruits = ["banana", "apple", "cherry", "mango", "strawberry"];
var points = [40, 100, 1, 5, 25, 10];
var cars = [
    {type: "Volvo", year: 2016},
    {type: "Saab", year: 2001},
    {type: "BMW", year: 2010}
];
var numbers = [45, 4, 9, 16, 25];
var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];


function listFruits(value) {
    console.log(fruits.constructor);
}

function mySort() {
    document.getElementById('mySort').innerHTML = fruits.sort();
    document.getElementById('myPoints').innerHTML =
        points.sort((a, b) => {
            return a - b;
        });


    var sortedByYear = cars.sort((a, b) => {
        return a.year - b.year;
    });

    document.getElementById('myCars').innerHTML = JSON.stringify(sortedByYear);
    var sortedByName = cars.sort((a, b) => {
        var x = a.type.toLowerCase();
        var y = b.type.toLowerCase();
        if (x < y) return -1;
        if (x > y) return 1;
        return 0;

    });
    document.getElementById('myCars2').innerHTML = JSON.stringify(sortedByName);

}

function myFilter() {

    // numbers.filter( (value, index, array) => value > 18); // also possible

    document.getElementById('myFilter').innerHTML = numbers;

    var fullAged = numbers.filter(value => value > 18);
    document.getElementById('fullAged').innerHTML = fullAged;

}

function myReduce() {
    document.getElementById('notReduced').innerHTML = numbers;
    var sum = numbers.reduce(doSum, 100)
    document.getElementById('reduce').innerHTML = 'Sum => ' + sum;
}

function doSum(total, value) {
    return total + value;
}

function myDate() {
    var dateAsString = '2020-04-21T17:50:00-06:00';
    var date = new Date(dateAsString);
    document.getElementById('date').innerHTML = date;
    Math.document.getElementById('utcDay').innerHTML = date.toUTCString();
}

function dateMethod() {
    var date = new Date();
    document.getElementById('dateMethod').innerHTML = days[date.getDay()];
}

function myRandom() {

    var begin = '<ul>';
    var content = '';
    var min = 3;
    var max = 10;
    for (var i = 0; i < 20; i++) {
        var random = Math.random();
        content += '<li>'
            + random + ' '
            + ' => '
            + getCustomRandom(min, max, random)
            + '</li>';
    }
    debugger;
    var end = '</ul>';
    document.getElementById('random').innerHTML = begin + content + end;
}

function getCustomRandom(min, max, random) {
    return Math.floor(random * (max - min)) + min;
}

function myHoisting() {
    x = 5;
    document.getElementById('hoisting').innerHTML = x;
    var x;
}

function hello() {
    alert(this);
}

function changeBrand() {
    var myCar = new Car('VW', 'Passat');
    let brand = document.getElementById('text').value;
    myCar.brand = brand;
    document.getElementById('classes').innerHTML = myCar.info();
}

function validateForm() {
    var x = document.forms['myForm']['fname'].value;
    console.log(' => ' + x);
    if (x === '') {
        alert('Name must be filled out')
    }
}

window.addEventListener("load", () => {
    var myCar = new Car('VW', 'Passat');
    document.getElementById('classes').innerHTML = myCar.info();
});

