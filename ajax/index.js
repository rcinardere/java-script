function getPersons() {

    var url = 'http://localhost:8091/persons';
    makeHttpRequest('GET', url, (xhttp) => {
        var body = makeBody(xhttp);
        document.getElementById('persons').innerHTML = body;
    });

}

function addUser(prename, surname) {

    var staff = {preName: prename, surName: surname};
    var url = 'http://localhost:8091/persons';
    makeHttpRequest('POST', url, staff,(xhttp) => {
        console.log(xhttp.responseText);
    });

}

function makeHeader(xhttp) {

    // header
    var table = "<table>" + "<tr>" +
        "<th>Key</th>" +
        "<th>Value</th>" +
        "</tr>";

    // 13 == '\r' == ASCII code for carriage return
    var seperator = String.fromCharCode(13);


    var headers = xhttp.getAllResponseHeaders();
    var splitted = headers.split(seperator);

    // body
    splitted.forEach(header => {
        var line = header.split(':');
        table += "<tr>"
            + "<td>" + line[0] + "</td>"
            + "<td>" + line[1] + "</td>"
            + "</tr>"
    })

    return table;
}

function makeBody(xhttp) {

    var persons = JSON.parse(xhttp.responseText);
    var table =
        "<table>" + "<tr>" +
        "<th>Prename</th>" +
        "<th>Surname</th>" +
        "<th>Username</th>" +
        "<th>PersonalId</th>" +
        "</tr>";

    if (Array.isArray(persons)) {
        persons.forEach(person => {
            table += makeRow(person);
        });
    } else {
        table += makeRow(persons);
    }

    table += "</tr></table>";
    return table;
}

function makeRow(person) {
    return "<tr><td>" + person.preName + "</td>"
        + "<td>" + person.surName + "</td>"
        + "<td>" + person.username + "</td>"
        + "<td>" + person.personalId + "</td></tr>";
}

function lookUp(username) {
    if (username.length >= 5) {
        var url = 'http://localhost:8091/persons/username/' + username;
        makeHttpRequest('GET', url, (xhttp) => {

            var person = makeBody(xhttp);
            document.getElementById('persons').innerHTML = person;
        });
    }
}

function makeHttpRequest(httpMethod, url, callBack) {

    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById('headers').innerHTML = makeHeader(xhttp);
            callBack(this);
        }
    };

    xhttp.open(httpMethod, url, true);
    xhttp.setRequestHeader('Authorization', 'Basic cmFtYXphbjpwYXNz');
    xhttp.setRequestHeader("Content-type", "application/json")
    xhttp.send();
}

function makeHttpRequest(httpMethod, url, body, callBack) {

    var xhttp = new XMLHttpRequest();

    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById('headers').innerHTML = makeHeader(xhttp);
            callBack(this);
        }
    };

    xhttp.open(httpMethod, url, true);
    xhttp.setRequestHeader('Authorization', 'Basic cmFtYXphbjpwYXNz');
    xhttp.setRequestHeader("Content-type", "application/json")
    xhttp.send(JSON.stringify(body));
}